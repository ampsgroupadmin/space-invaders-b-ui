import { Event } from "robotlegs";
import { RoomList } from "../../types/room_list";
import { HighscoreList } from "../../types/highscore_list";

export class GameUpdateEvent extends Event {

    public static readonly GAME_UPDATED: string = "GAME_UPDATED";

    constructor() {
        super(GameUpdateEvent.GAME_UPDATED);
    }

}
