import {Event, IEvent} from "robotlegs";
import {IEventInit} from "robotlegs/lib/robotlegs/bender/events/api/IEvent";

export class ModelEvent extends Event {

    public static readonly CHANGE = "CHANGE";

    constructor(type: string, eventInit: IEventInit = { bubbles: false }) {
        super(type, eventInit);
    }

}