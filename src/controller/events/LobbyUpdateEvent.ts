import { Event } from "robotlegs";
import { RoomList } from "../../types/room_list";
import { HighscoreList } from "../../types/highscore_list";

export class LobbyUpdateEvent extends Event {

    public static readonly ROOMS_UPDATED: string = "ROOMS_UPDATED";
    public static readonly HIGHSCORE_UPDATED: string = "HIGHSCORE_UPDATED";

    constructor(type: string, detail: RoomList | HighscoreList) {
        super(type);
        this.detail = detail;
    }

}
