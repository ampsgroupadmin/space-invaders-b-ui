import {Event, IEvent} from "robotlegs";
import {IEventInit} from "robotlegs/lib/robotlegs/bender/events/api/IEvent";
import {MovieClip} from "pixi-animate";

export class StartupEvent extends Event {

    public static readonly STARTUP = "STARTUP";
    public static readonly STARTUP_COMPLETE = "STARTUP_COMPLETE";

    public instance: MovieClip;

    constructor(type: string, instance: MovieClip = null, eventInit: IEventInit = { bubbles: false }) {
        super(type, eventInit);

        this.instance = instance;
    }

}