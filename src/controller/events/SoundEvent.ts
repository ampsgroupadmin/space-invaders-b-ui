import {Event} from "robotlegs";
import {IEventInit} from "robotlegs/lib/robotlegs/bender/events/api/IEvent";

export class SoundEvent extends Event {

    public static readonly PLAY = "PLAY";

    public id: string;

    constructor(type: string, id: string = null, eventInit: IEventInit = {bubbles: false}) {
        super(type, eventInit);

        this.id = id;
    }

}