import { Event } from "robotlegs";

export class StartGameEvent extends Event {

    public static readonly START_GAME = "START_GAME";

    constructor() {
        super(StartGameEvent.START_GAME);

    }

}

