import { IEventDispatcher, inject, injectable } from "robotlegs";
import { StartupEvent } from "../events/StartupEvent";
import { LobbyService } from "../../service/LobbyService";
import MovieClip = PIXI.animate.MovieClip;

@injectable()
export class StartupCommand {

    @inject(StartupEvent)
    public event: StartupEvent;

    @inject(IEventDispatcher)
    public eventDispatcher: IEventDispatcher;

    @inject(LobbyService)
    public lobbyService: LobbyService;

    private instance: any;

    public execute(): void {
        setTimeout(() => this.lobbyService.requestNewRoom(Math.random().toString()), 5000);
        this.loadAssets();
    }

    private loadAssets() {
        let loader = new PIXI.loaders.Loader();

        // loader
            // .add("game_start", "assets/sounds/game_start.ogg")
            // .add("spin_ready", "assets/sounds/spin_ready.ogg")
            // .add("spin_loop", "assets/sounds/spin_loop.ogg")
            // .add("spin_stop", "assets/sounds/spin_stop.ogg")
            // .add("game_end", "assets/sounds/game_end.ogg")
            // already loaded in animate
            // .add("spin_start", "assets/sounds/spin_start.ogg")
            // .add("spin_tick", "assets/sounds/spin_tick.ogg")
            // .add("win_loop", "assets/sounds/win_loop.ogg")
            // .add("font_dark", "assets/fonts/font_dark.fnt")
            // .add("font_light", "assets/fonts/font_light.fnt")
            // .load(function (loader, resources) {
                this.loadAnimateJS();
            // }.bind(this));
    }

    private loadAnimateJS() {
        let js = document.createElement("script");
        js.onload = function () {
            this.loadAnimateScene();
        }.bind(this);

        js.type = "text/javascript";
        js.src = "assets/ui.js";

        document.body.appendChild(js);
    }

    private loadAnimateScene() {
        let lib: any = (<any>window).lib;

        PIXI.animate.load(lib.ui, null, function (instance) {
            this.complete(instance);
        }.bind(this), "assets/");
    }


    private complete(instance: MovieClip) {
        this.eventDispatcher.dispatchEvent(new StartupEvent(StartupEvent.STARTUP_COMPLETE, instance));
    }
}