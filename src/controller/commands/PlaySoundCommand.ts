import {IEventDispatcher, inject, injectable} from "robotlegs";
import {SoundEvent} from "../events/SoundEvent";
import sound from "pixi-sound";
import {DataModel} from "../../model/DataModel";

@injectable()
export class PlaySoundCommand {

    @inject(SoundEvent)
    public event: SoundEvent;

    @inject(IEventDispatcher)
    public eventDispatcher: IEventDispatcher;

    @inject(DataModel)
    public dataModel: DataModel;

    public execute(): void {
        let id: string = this.event.id;
        // TODO: create sound manager which stores current sound and can stop it by id
        sound.play(id);
    }
}