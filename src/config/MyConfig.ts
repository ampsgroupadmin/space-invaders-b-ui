import {IConfig, IEventCommandMap, IInjector, inject, injectable} from "robotlegs";
import {IContextView, IMediatorMap} from "robotlegs-pixi";
import {DataModel} from "../model/DataModel";
import {StartupEvent} from "../controller/events/StartupEvent";
import {MainMediator} from "../view/mediators/MainMediator";
import {MainView} from "../view/MainView";
import {StartupCommand} from "../controller/commands/StartupCommand";
import {LobbyScreen} from "../view/components/LobbyScreen";
import {LobbyScreenMediator} from "../view/mediators/LobbyScreenMediator";
import {LobbyService} from "../service/LobbyService";
import {LobbyModel} from "../model/LobbyModel";
import {CreateRoomPopupMediator} from "../view/mediators/CreateRoomPopupMediator";
import {CreateRoomPopup} from "../view/popup/CreateRoomPopup";

@injectable()
export class MyConfig implements IConfig {

    @inject(IInjector)
    public injector: IInjector;

    @inject(IMediatorMap)
    public mediatorMap: IMediatorMap;

    @inject(IEventCommandMap)
    public commandMap: IEventCommandMap;

    @inject(IContextView)
    public contextView: IEventCommandMap;

    public configure() {

        // Models
        this.injector.bind(DataModel).toSelf().inSingletonScope();
        this.injector.bind(LobbyModel).toSelf().inSingletonScope();

        // Commands
        this.commandMap.map(StartupEvent.STARTUP).toCommand(StartupCommand);

        // Views
        this.mediatorMap.map(MainView).toMediator(MainMediator);
        this.mediatorMap.map(LobbyScreen).toMediator(LobbyScreenMediator);
        this.mediatorMap.map(CreateRoomPopup).toMediator(CreateRoomPopupMediator);

        // Service
        this.injector.bind(LobbyService).toSelf().inSingletonScope();
    }

}
