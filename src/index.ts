import "reflect-metadata";
import {Context, MVCSBundle} from "robotlegs";
import {ContextView, PixiBundle} from "robotlegs-pixi";

import "pixi-animate";

import {MyConfig} from "./config/MyConfig";
import {MainView} from "./view/MainView";

import "pixi-tween";
import "pixi-sound";

export class Main {

    public stage: PIXI.Container;
    public renderer: PIXI.CanvasRenderer | PIXI.WebGLRenderer;
    public context: Context;
    public rendererWidth = 1080;
    public rendererHeight = 1920;
    public view: MainView;

    constructor(containerID = "canvasBox") {

        this.renderer = PIXI.autoDetectRenderer(this.rendererWidth, this.rendererHeight, {});
        document.getElementById(containerID).appendChild(this.renderer.view);

        this.stage = new PIXI.Container();

        this.context = new Context();
        this.context.install(MVCSBundle, PixiBundle).configure(new ContextView((<any>this.renderer).plugins.interaction)).configure(MyConfig).initialize(this._onContextReady.bind(this));

        this.view = new MainView();
        this.view.on(MainView.CHANGE_DISPLAY, this._onChangeDisplay, this)
        this.stage.addChild(this.view);

        PIXI.ticker.shared.add(this._onTickEvent, this);

        this.render();
    }

    public render = () => {
        this.renderer.render(this.stage);
        window.requestAnimationFrame(this.render);
    }

    private _onTickEvent(deltaTime) {
        if (PIXI.tweenManager) {
            PIXI.tweenManager.update(deltaTime / 60);
        }
    }

    private _onChangeDisplay(rendererWidth, rendererHeight) {
        this.renderer.resize(rendererWidth, rendererHeight);
    }

    private _onContextReady() {

    }

}
