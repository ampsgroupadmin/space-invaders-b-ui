import {Highscore} from "./highscore";

export type HighscoreList = ReadonlyArray<Highscore>;
