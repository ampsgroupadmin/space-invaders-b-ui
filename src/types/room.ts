export type Room = Readonly<{
    roomId: string;
    players?: ReadonlyArray<string>;
    level?: number;
}>;
