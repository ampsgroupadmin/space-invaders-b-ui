export type Player = Readonly<{
    playerId: string;
    position: Position;
    health: number;
    score: number;
}>;
