export type Shot = Readonly<{
    position: Position;
    strength: number;
    type: string;
    shooterId: string;
    direction: string;
}>;

