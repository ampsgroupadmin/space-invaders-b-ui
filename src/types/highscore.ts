export type Highscore = Readonly<{
    playerId: string;
    score: number;
}>;
