import { Monster } from "./monster";
import {Player} from "./player";
import {Shot} from "./shot";

export type GameState = Readonly<{
    config: Readonly<{
        level: number;
        roomName: string;
    }>;
    monsters: ReadonlyArray<Monster>;
    players: ReadonlyArray<Player>;
    shots: ReadonlyArray<Shot>;
}>;
