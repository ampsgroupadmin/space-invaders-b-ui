export type Monster = Readonly<{
    position: Position;
    health: number;
    type: string;
    monsterId: string;
    moveDirection: string;
}>;
