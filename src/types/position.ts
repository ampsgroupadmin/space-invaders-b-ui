export type position = Readonly<{
    x: number;
    y: number;
}>;
