import {HighscoreList} from "./highscore_list";
import {RoomList} from "./room_list";

export type LobbyState = Readonly<{
    lobby: Readonly<{
        highscores: HighscoreList;
        rooms: RoomList;
    }>;
}>;
