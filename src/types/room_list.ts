import { Room } from "./Room";

export type RoomList = ReadonlyArray<Room>;
