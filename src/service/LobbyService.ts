import { inject, injectable } from "robotlegs";
import { LobbyModel } from "../model/LobbyModel";
import { LobbyState } from "../types/lobby_state";

@injectable()
export class LobbyService {

    private _websocket: WebSocket;

    @inject(LobbyModel)
    private lobbyModel: LobbyModel;

    public requestNewRoom(roomId: string): Promise<null> {
        return new Promise((resolve, reject) => {
            this.connect().then(
                () => {
                    this._websocket.send(JSON.stringify({
                        roomCreate: { roomId }
                    }));
                    resolve();
                }
            );
        });
    }

    private connect(): Promise<null> {
        return new Promise((resolve, reject) => {
            if (this.connected) {
                return resolve(null);
            }

            this._websocket = new WebSocket("ws://localhost:8080/lobby");

            const onOpen = () => {
                this._websocket.removeEventListener("open", onOpen);
                resolve();
            };

            this._websocket.addEventListener("message", this.parseMessage);
            this._websocket.addEventListener("open", onOpen);
        });
    }

    private parseMessage = (message: MessageEvent): void => {
        const data = JSON.parse(message.data);

        if (data.lobby) {
            this.lobbyModel.addRooms(...(<LobbyState>data).lobby.rooms);
            this.lobbyModel.highscores = (<LobbyState>data).lobby.highscores;
        } else if (data.roomUpdate) {
            this.lobbyModel.addRooms(data.roomUpdate);
        }
        console.log(data);
    }

    public get connected(): boolean {
        return this._websocket && this._websocket.readyState === 1;
    }

}
