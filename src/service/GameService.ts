import { inject, injectable } from "robotlegs";
import { GameModel } from "../model/GameModel";
import { GameState } from "../types/game_state";
import {Direction} from "../types/direction";

@injectable()
export class LobbyService {

    private _websocket: WebSocket;

    @inject(GameModel)
    private gameModel: GameModel;

    public connect(roomId: string, playerId: string): Promise<null> {
        return new Promise((resolve, reject) => {
            if (this.connected) {
                return resolve(null);
            }

            this._websocket = new WebSocket(
                `ws://localhost:8080/game?roomId=${roomId}&$playerId=${playerId}`
            );

            const onOpen = () => {
                this._websocket.removeEventListener("open", onOpen);
                resolve();
            };

            this._websocket.addEventListener("open", onOpen);
            this._websocket.addEventListener("message", this.parseMessage);

        });
    }

    public disconnect(): void {
        if (!this.connected) {
            return;
        }

        this._websocket.removeEventListener("open");
        this._websocket.removeEventListener("message");
        this._websocket.close();
    }

    public move(direction: Direction): void {
        if (!this.connected) {
            return;
        }

        this._websocket.send({
            move: direction
        });
    }

    public shoot(): void {
        if (!this.connected) {
            return;
        }

        this._websocket.send({
            shoot: true
        });
    }

    private parseMessage = (message: MessageEvent): void => {
        const data: GameState = JSON.parse(message.data);
        this.gameModel.level = data.config.level;
        this.gameModel.roomName = data.config.roomName;
        this.gameModel.monsters = data.monsters;
        this.gameModel.players = data.players;
        this.gameModel.shots = data.shots;
        this.gameModel.dispatchUpdate();
        console.log(data)
    }

    public get connected(): boolean {
        return this._websocket && this._websocket.readyState === 1;
    }

}