import {UIUtils} from "../../utils/UIUtils";

export class CreateRoomPopup extends PIXI.Container {

    public static EVENT_TRIGGERED = "EVENT_TRIGGERED";

    private root;
    private btnCreate;
    private input;

    constructor() {
        super();

        this.root = new window["lib"].CreateRoomPopup();
        this.addChild(this.root);

        this.btnCreate = this.root.btnCreate;
        this.input = this.root.input;

        UIUtils.buttonize(this.btnCreate, function () {
            this.emit(CreateRoomPopup.EVENT_TRIGGERED);
        }.bind(this));

    }

    public getRoomName(): string {
        return this.input.text;
    }

}
