import {Mediator} from "robotlegs-pixi";
import {StartupEvent} from "../../controller/events/StartupEvent";
import {MainView} from "../MainView";

export class MainMediator extends Mediator<MainView> {

    public initialize() {
        this.eventDispatcher.addEventListener(StartupEvent.STARTUP_COMPLETE, this.onStartupComplete, this);
        this.eventDispatcher.dispatchEvent(new StartupEvent(StartupEvent.STARTUP));
    }

    public destroy() {

    }

    private onViewChangeDisplay(rendererWidth, rendererHeight) {

    }

    private onStartupComplete(event: StartupEvent) {
        this.eventDispatcher.removeEventListener(StartupEvent.STARTUP_COMPLETE, this.onStartupComplete, this);
        this.view.start(event.instance);

        // this.view.on("children_ready", this.onViewChildrenRedy, this);
    }

    private onViewChildrenRedy() {
        this.eventDispatcher.dispatchEvent(new StartupEvent(StartupEvent.STARTUP));
    }
}