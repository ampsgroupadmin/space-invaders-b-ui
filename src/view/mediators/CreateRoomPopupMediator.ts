import {Mediator} from "robotlegs-pixi";
import {IEvent, IEventDispatcher, inject} from "robotlegs";
import {CreateRoomPopup} from "../popup/CreateRoomPopup";
import {LobbyService} from "../../service/LobbyService";

export class CreateRoomPopupMediator extends Mediator<CreateRoomPopup> {

    @inject(LobbyService)
    public lobbyService: LobbyService;

    @inject(IEventDispatcher)
    public eventDispatcher: IEventDispatcher;

    public initialize() {
        this.view.on(CreateRoomPopup.EVENT_TRIGGERED, this.onEventTriggered, this);
    }

    public destroy() {
        this.view.off(CreateRoomPopup.EVENT_TRIGGERED, this.onEventTriggered, this);
    }

    private onEventTriggered(e: IEvent) {
        this.lobbyService.requestNewRoom(this.view.getRoomName());
    }

}
