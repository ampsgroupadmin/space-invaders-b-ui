import {Mediator} from "robotlegs-pixi";
import {LobbyScreen} from "../components/LobbyScreen";
import { IEventDispatcher, inject, injectable } from "robotlegs";
import {LobbyModel} from "../../model/LobbyModel";

export class LobbyScreenMediator extends Mediator<LobbyScreen> {

    @inject(LobbyModel)
    public lobbyModel: LobbyModel;

    @inject(IEventDispatcher)
    public eventDispatcher: IEventDispatcher;


    public initialize() {
        // this.eventDispatcher.addEventListener()

        // this.view.showRooms();
    }

    public destroy() {

    }
}