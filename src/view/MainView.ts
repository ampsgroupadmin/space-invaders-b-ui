import {Container} from "pixi.js";
import {LobbyScreen} from "./components/LobbyScreen";

export class MainView extends Container {

    public static CHANGE_DISPLAY = "CHANGE_DISPLAY";

    // private wheel: Wheel;
    private lib: object;
    private _queue: Array<object> = [];
    private _queueIsRunning: boolean = false;

    constructor() {
        super();

        //TODO : add screen container POC

        // this.wheel = new Wheel();

        // TODO: move logic to command
        // this.wheel.on("initialized", this._executeNextAction, this);
    }

    public start(instance): void {
        instance.removeChild(instance.getChildByName("exports"));

        let lobbyScreen: LobbyScreen = new LobbyScreen(new window["lib"].lobby());
        this.addChild(lobbyScreen);
    }


}
