var ResourcesScene = function (id, libName, assetName, url) {
	BaseScene.call(this, id, libName, assetName, url);
};

ResourcesScene.prototype = Object.create(BaseScene.prototype);

ResourcesScene.prototype.constructor = ResourcesScene;

ResourcesScene.prototype.reset = function () {
	// this.root.gotoAndStop(0);
};

ResourcesScene.prototype._complete = function (instance) {
	
	this.root = instance;
	this.addChild(this.root);
	
	// this.root.gotoAndStop(0);
	
	this.initScreens();
	
	this.showScreen("MainMenu", "up");
};

ResourcesScene.prototype.initScreens = function () {
	
	this.screenIDs = ["MainMenu", "rolesScreen", "aggressionScreen", "seekHelp", "contactNumbers", "respondingSituations", "emergencies", "consultation"];
	this.screens = {};
	var id;
	var screen;
	
	for (var i = 0; i < this.screenIDs.length; i++) {
		id = this.screenIDs[i];
		screen = this.root.getChildByName(id);
		
		this.screens[id] = screen;
		this.root.removeChild(screen);
		
		if (screen.getChildByName("btn_back")) {
			this.buttonize(screen.getChildByName("btn_back"), function () {
				this.showScreen("MainMenu", "right");
			}.bind(this));
		}
	}
	
	this.initMenu();
	this.createPaging("seekHelp", 2);
	this.createPaging("emergencies", 3);
	this.createPaging("consultation", 2);
	
};

ResourcesScene.prototype.createPaging = function (id, count) {
	var screen = this.screens[id];
	if (!screen) return;
	
	var pages = screen.getChildByName("pages");
	
	for (var i = 0; i < count; i++) {
		screen.getChildByName("p" + i).id = i;
		this.buttonize(screen.getChildByName("p" + i), function (btn) {
			createjs.Tween.get(pages, {override: true}).wait(0).to({
				x: btn.id * -2048
			}, 750, createjs.Ease.quadInOut);
			
			
		}, "group");
	}
	
	screen.getChildByName("p0").gotoAndStop(1);
	
};

ResourcesScene.prototype.showScreen = function (id, direction) {
	var screen = this.screens[id];
	
	if (!screen) return;
	
	if (this.isTransitioning) return;
	
	if (this.currentScreen && this.currentScreen === screen) return;
	
	var fromP;
	var toP = new PIXI.Point(0, 0);
	var outP;
	
	switch (direction) {
		case "up":
			fromP = new PIXI.Point(0, 1536);
			outP = new PIXI.Point(0, -1536);
			break;
		case "down":
			fromP = new PIXI.Point(0, -1536);
			outP = new PIXI.Point(0, 1536);
			break;
		case "left":
			fromP = new PIXI.Point(2048, 0);
			outP = new PIXI.Point(-2048, 0);
			break;
		case "right":
			fromP = new PIXI.Point(-2048, 0);
			outP = new PIXI.Point(2048, 0);
			break;
	}
	
	this.isTransitioning = true;
	
	
	screen.x = fromP.x;
	screen.y = fromP.y;
	
	this.root.addChild(screen);
	
	if(screen.getChildByName("pages")) {
		screen.getChildByName("pages").x = 0;
		screen.getChildByName("p0").gotoAndStop(1);
	}
	
	createjs.Tween.get(screen, {override: true}).wait(0).to({
		x: toP.x,
		y: toP.y
	}, 750, createjs.Ease.quadInOut).call(function () {
		this.isTransitioning = false;
		this.root.removeChild(this.currentScreen);
		this.currentScreen = screen;
	}.bind(this));
	
	if (this.currentScreen) {
		var prevScreen = this.currentScreen;
		createjs.Tween.get(prevScreen, {override: true}).wait(0).to({
			x: outP.x,
			y: outP.y
		}, 750, createjs.Ease.quadInOut).call(function () {
			this.root.removeChild(prevScreen);
		}.bind(this));
	}
	
	
};

ResourcesScene.prototype.initMenu = function () {
	var MainMenu = this.screens["MainMenu"];
	
	this.buttonize(MainMenu.getChildByName("btn_t1_0"), function () {
		// URL
	}.bind(this));
	
	
	this.buttonize(MainMenu.getChildByName("btn_t2_0"), function () {
		this.showScreen("rolesScreen", "left");
	}.bind(this));
	
	
	this.buttonize(MainMenu.getChildByName("btn_t3_0"), function () {
		// URL
	}.bind(this));
	
	this.buttonize(MainMenu.getChildByName("btn_t3_1"), function () {
		this.showScreen("aggressionScreen", "left");
	}.bind(this));
	
	this.buttonize(MainMenu.getChildByName("btn_t3_2"), function () {
		this.showScreen("seekHelp", "left");
	}.bind(this));
	
	this.buttonize(MainMenu.getChildByName("btn_t3_3"), function () {
		//TODO: NOT IN ASSSETS!!!!!
	}.bind(this));
	
	this.buttonize(MainMenu.getChildByName("btn_t3_4"), function () {
		this.showScreen("contactNumbers", "left");
	}.bind(this));
	
	
	this.buttonize(MainMenu.getChildByName("btn_t4_0"), function () {
		this.showScreen("respondingSituations", "left");
	}.bind(this));
	
	this.buttonize(MainMenu.getChildByName("btn_t4_1"), function () {
		this.showScreen("emergencies", "left");
	}.bind(this));
	
	this.buttonize(MainMenu.getChildByName("btn_t4_2"), function () {
		this.showScreen("consultation", "left");
	}.bind(this));
	
};


ResourcesScene.prototype.buttonize = function (target, callback, selectGroup) {
	target.gotoAndStop && target.gotoAndStop(0);
	
	target.interactive = true;
	target.buttonMode = true;
	
	var isOver = false;
	
	if (selectGroup) {
		if (!this.selectGroups) {
			this.selectGroups = {};
		}
		if (!this.selectGroups[selectGroup]) {
			this.selectGroups[selectGroup] = [];
		}
		
		this.selectGroups[selectGroup].push(target);
	}
	
	var buttonDown = function (e) {
		target.gotoAndStop && target.gotoAndStop(1);
	}.bind(this);
	
	var buttonOver = function (e) {
		target.gotoAndStop && target.gotoAndStop(1);
	}.bind(this);
	
	var buttonUp = function (e) {
		if (target.isSelected) return;
		target.gotoAndStop && target.gotoAndStop(0);
	}.bind(this);
	
	target.on('pointerdown', function (e) {
		if (selectGroup) {
			
			if(this.selectGroups[selectGroup]) {
				for (var i = 0; i < this.selectGroups[selectGroup].length; i++) {
					this.selectGroups[selectGroup][i].isSelected = false;
					this.selectGroups[selectGroup][i].gotoAndStop && this.selectGroups[selectGroup][i].gotoAndStop(0);
				}
			}
			
			target.isSelected = true;
		}
		buttonDown();
		callback(target);
	}.bind(this));
	
	target.on('pointerup', function (e) {
		if (isOver) return;
		buttonUp();
	}.bind(this));
	
	target.on('mouseover', function (e) {
		isOver = true;
		buttonOver();
	}.bind(this));
	
	target.on('mouseout', function (e) {
		isOver = false;
		buttonUp();
	}.bind(this));
};

