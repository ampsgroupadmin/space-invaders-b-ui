import {EventDispatcher, IEventDispatcher} from "robotlegs";
import {RoomItem} from "./RoomItem";
import {Room} from "../../types/room";
import {UIUtils} from "../../utils/UIUtils";

export class LobbyScreen extends PIXI.Container {

    private root;
    private btnHiscore;
    private btnNewRoom;
    private rooms: PIXI.Container;

    constructor(root: any) {
        super();

        this.root = root;
        this.addChild(this.root);

        this.btnHiscore = this.root.btnHiscore;
        this.btnNewRoom = this.root.btnNewRoom;

        UIUtils.buttonize(this.btnHiscore, function () {
            alert("sdfdsf");
        }.bind(this));

        UIUtils.buttonize(this.btnNewRoom, function () {
            alert("add room");
        }.bind(this));

        this.rooms = new PIXI.Container();
        this.rooms.y = 580;
        this.addChild(this.rooms);

    }


    public showRooms(rooms: ReadonlyArray<Room>) {
        this.rooms.removeChildren();
        let item: RoomItem;
        let posy = 0;
        for (let i; i < rooms.length; i++) {
            item = new RoomItem(rooms[i].roomId);
            this.rooms.addChild(item);
            item.y = posy;
            posy += item.height;
        }
    }

}
