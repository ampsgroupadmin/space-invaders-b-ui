export class RoomItem extends PIXI.Container {

    private root;
    private ico;
    private label;

    constructor(roomName: string) {
        super();

        this.root = new window["lib"].roomItem();
        this.addChild(this.root);

        this.ico = this.root.ico;
        this.label = this.root.label;
        this.ico.gotoAndStop(Math.floor(Math.random() * 3));

        this.label.text = roomName;

    }
}