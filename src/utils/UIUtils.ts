export class UIUtils  {

    public static buttonize = function (target, callback) {
        target.gotoAndStop && target.gotoAndStop(0);

        target.interactive = true;
        target.buttonMode = true;

        let isOver = false;

        let buttonDown = function (e) {
            target.gotoAndStop(2);
        }.bind(this);

        let buttonOver = function (e) {
            target.gotoAndStop(1);
        }.bind(this);

        let buttonUp = function (e) {
            target.gotoAndStop(0);
        }.bind(this);

        target.on('pointerdown', function (e) {
            buttonDown();
            callback();
        }.bind(this));

        target.on('pointerup', function (e) {
            if (isOver) return;
            buttonUp();
        }.bind(this));

        target.on('mouseover', function (e) {
            isOver = true;
            buttonOver();
        }.bind(this));

        target.on('mouseout', function (e) {
            isOver = false;
            buttonUp();
        }.bind(this));
    };

}
