const enum LevelType {
    ONYX = 0,
    SAPPHIRE = 1,
    RUBY = 2,
    DIAMOND = 3
}