import { IEventDispatcher, inject, injectable } from "robotlegs";
import { HighscoreList } from "../types/highscore_list";
import { LobbyUpdateEvent } from "../controller/events/LobbyUpdateEvent";
import { Room } from "../types/room";

@injectable()
export class LobbyModel {

    private _rooms: Map<string, Room> = new Map();
    private _highscores: HighscoreList = [];

    @inject(IEventDispatcher)
    public eventDispatcher: IEventDispatcher;

    public addRooms(...rooms: Array<Room>) {
        rooms.forEach(room => this._rooms.set(room.roomId, room));
    }

    public get rooms(): ReadonlyMap<string, Room> {
        return this._rooms;
    }

    public get highscores(): HighscoreList {
        return this._highscores;
    }

    public set highscores(highscores: HighscoreList) {
        this._highscores = highscores;
        this.eventDispatcher.dispatchEvent(
            new LobbyUpdateEvent(LobbyUpdateEvent.HIGHSCORE_UPDATED, highscores)
        );
    }

}
