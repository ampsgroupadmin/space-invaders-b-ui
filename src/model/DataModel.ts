import {IEventDispatcher, inject, injectable} from "robotlegs/lib";
import {ModelEvent} from "../controller/events/ModelEvent";

@injectable()
export class DataModel {

    @inject(IEventDispatcher)
    public eventDispatcher: IEventDispatcher;

    constructor() {
    }
}