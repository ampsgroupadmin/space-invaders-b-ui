import { IEventDispatcher, inject, injectable } from "robotlegs";
import { GameUpdateEvent } from "../controller/events/GameUpdateEvent";
import {Monster} from "../types/monster";
import {Player} from "../types/player";
import {Shot} from "../types/shot";

@injectable()
export class GameModel {

    public level = 0;
    public roomName: string;
    public monsters: ReadonlyArray<Monster> = [];
    public players: ReadonlyArray<Player> = [];
    public shots: ReadonlyArray<Shot> = [];

    @inject(IEventDispatcher)
    public eventDispatcher: IEventDispatcher;

    public dispatchUpdate(): void {
        this.eventDispatcher.dispatchEvent(new GameUpdateEvent());
    }

}
