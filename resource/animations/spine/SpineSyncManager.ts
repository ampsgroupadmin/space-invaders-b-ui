import { SpineState } from "./SpineState";
import { isArray } from "util";

export class SpineSyncManager {

	/** Current state name for all synced spine containers */
	private static _currentState: string = null;

	/** Current animation name for all synced spine containers */
	private static _currentAnimationName: string = null;

	public static getAnimationName(state: SpineState) {

		if (state && this._currentState !== state.name) {
			this._currentAnimationName = state.animationNames[Math.floor(Math.random() * state.animationNames.length)];
			this._currentState = state.name;
		}

		return this._currentAnimationName;

	}
}
