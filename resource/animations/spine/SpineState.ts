import { isArray, isString } from "util";
import EventEmitter = PIXI.utils.EventEmitter;

export class SpineState extends EventEmitter {

	public static readonly SHOW_STATE = "SHOW_STATE";

	/** name of this state */
	public name: string = null;

	/** list of animation names */
	public animationNames: Array<string> = [];

	/** animation loop */
	public loop: boolean = false;

	/** state name to be played after this animation is finished, NOT animation name. onComplete won't fire when loop is true */
	public onComplete: string = null;

	/** event name upon which this state will be played */
	public event: string = null;

	constructor(name, config) {
		super();

		this.name = name;

		Object.assign(this, config);
	}

	public showState() {
		this.emit(SpineState.SHOW_STATE, { state: this.name });
	}

	get animationName() {
		throw new Error("SpineState :: you can only set animtionName");
	}

	set animationName(val: any) {

		if (isArray(val)) {
			this.animationNames = val;
		} else if (isString(val)) {
			this.animationNames = [val];
		} else {
			throw new Error("SpineState :: unsupported value type");
		}


	}

}
