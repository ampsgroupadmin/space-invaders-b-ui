import {Mediator} from "robotlegs-pixi";
import {SpineContainer} from "../SpineContainer";
import {SpineState} from "../SpineState";

export class SpineContainerMediator extends Mediator<SpineContainer> {

    public initialize() {

        this.view.on(SpineContainer.ANIMATION_COMPLETE, this.onAnimationComplete, this);
        this.view.on(SpineContainer.SPINE_EVENT, this.onSpineEvent, this);

        if (this.view.showOnLoad) {
            this.view.show();
        }

        this.view.spineStates.forEach((state: SpineState, key: string) => {
            if (state.event) {
                this.eventDispatcher.addEventListener(state.event, state.showState, state);
            }
        });
    }

    public destroy() {
        this.view.off(SpineContainer.ANIMATION_COMPLETE, this.onAnimationComplete, this);
        this.view.off(SpineContainer.SPINE_EVENT, this.onSpineEvent, this);

        this.view.spineStates.forEach((state: SpineState, key: string) => {
            if (state.event) {
                this.eventDispatcher.removeEventListener(state.event, state.showState, state);
            }
        });
    }

    public onAnimationComplete(event: any) {
        this.eventDispatcher.dispatchEvent(new SpineEvent(SpineEvent.ANIMATION_COMPLETE));
    }

    public onSpineEvent(event: Event) {
        this.eventDispatcher.dispatchEvent(new SpineEvent(SpineEvent.TRIGGERED));
    }

}
