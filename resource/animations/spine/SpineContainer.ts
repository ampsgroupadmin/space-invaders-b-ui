import PContainer from "../../components/containers/PContainer";
import {IAnimationContainer} from "../IAnimationContainer";
import {SpineState} from "./SpineState";
import {SpineSyncManager} from "./SpineSyncManager";
import {ViewClass} from "viewFactory";
import TextureAtlas = PIXI.spine.core.TextureAtlas;
import SkeletonJson = PIXI.spine.core.SkeletonJson;
import Spine = PIXI.spine.Spine;
import AtlasAttachmentLoader = PIXI.spine.core.AtlasAttachmentLoader;
import Point = PIXI.Point;
import TrackEntry = PIXI.spine.core.TrackEntry;

@ViewClass("SpineContainer")
export class SpineContainer extends PContainer implements IAnimationContainer {

    public static readonly ANIMATION_COMPLETE = "ANIMATION_COMPLETE";
    public static readonly SPINE_EVENT = "SPINE_EVENT";

    /** spine .json skeleton file ID*/
    public file: string;

    /** spine animation crossover mix time in seconds*/
    public animationCrossfadeTime: number = 0;

    /** show immediately after load */
    public showOnLoad: boolean = false;

    /** position of spine animaiton inside container */
    public innerPosition: Point = new Point(0, 0);

    /** scale of spine animaiton inside container */
    public innerScale: Point = new Point(1, 1);

    /** use synchronisation manager play all spine containers in current game with same animation name */
    public hasSyncManager: boolean = false;

    /** list of possible states */
    private _spineStates: Map<String, SpineState>;
    private _cfgStates;

    private _spine: Spine = null;
    private _defaultStateName: string = null;
    private _currentStateName: string = null;
    private _currentAnimationName: string = null;
    private _running: boolean = null;

    constructor(config) {
        super(config);

        Object.assign(this, config);

        if (!this.file) {
            throw new Error("SpineContainer :: You cannot create spine animation without spine config");
        }

        this._parse();
        this._build();
    }

    /**
     * unhides the spine object and plays currently chosen animation state
     */
    public show() {
        this.addChild(this._spine);
        this._showState();
        this.play();
    }

    /**
     * hides the spine object and resets current animation state do default state
     */
    public hide() {
        this.removeChild(this._spine);
        this.reset();
    }

    /**
     * plays current spine animation state, does nothing if animation is already running
     *
     */
    public play() {
        if (this._running) {
            return;
        }

        if (!this._currentAnimationName) {
            this._showState();
        }

        this._running = true;

        PIXI.ticker.shared.add(this._update, this);

        this._update();
    }

    /**
     * plays spine animation, does nothing if animation is already running
     * @param {string} state name of the state to be played
     */
    public playState(state: string) {
        this._currentStateName = state;
        this._showState();

        this.play();
    }

    /**
     * stops spine animation, does nothing if already stopped
     */
    public stop() {
        if (!this._running) {
            return;
        }

        this._running = false;

        PIXI.ticker.shared.remove(this._update, this);
    }

    /**
     * stops and resets spine animation, sets the current animation state to default
     */
    public reset() {
        this.stop();

        this._currentStateName = this._defaultStateName;

        this._currentAnimationName = null;
        this._spine.state.clearTracks();

        this._showState();

        this._update();
    }

    public destroy(): void {
        this._spineStates.forEach((state: SpineState, key: string) => {
            state.off(SpineState.SHOW_STATE, this._onSpineStateShow, this);
        });

        super.destroy();
    }

    private _parse() {

        //TODO: TEMP
        const cache: any = {};
        for (let attr in PIXI.utils.TextureCache) {
            cache[attr] = PIXI.utils.TextureCache[attr];
        }

        const atlas = new TextureAtlas();
        atlas.addTextureHash(cache, true);

        if (!PIXI.loader.resources[this.file]) {
            console.error("SPINE ASSET DOESN'T EXIST " + this.file);
        }

        const rawSkeletonData = PIXI.loader.resources[this.file].data;

        const spineJsonParser = new SkeletonJson(new AtlasAttachmentLoader(atlas));
        const skeletonData = spineJsonParser.readSkeletonData(rawSkeletonData);

        this._spine = new Spine(skeletonData);
    }

    private _build() {
        this._spine.skeleton.setToSetupPose();
        this._spine.update(0);
        this._spine.autoUpdate = false;

        if (this.animationCrossfadeTime > 0) {
            let i = 0;
            let j = 0;
            const animations = this._spine.skeleton.data.animations;
            const count = animations.length;

            for (i = 0; i < count; i++) {
                for (j = 0; j < count; j++) {
                    if (i === j) {
                        continue;
                    }
                    this._spine.stateData.setMixByName(animations[i].name, animations[j].name, this.animationCrossfadeTime);
                }
            }
        }

        this._spine.state.onEvent = (i, event) => {
            this._onSpineEventTriggered(i, event);
        };

        this._spine.scale.set(this.innerScale.x, this.innerScale.y);
        this._spine.position.set(this.innerPosition.x, this.innerPosition.y);

    }

    private _generateSpineStates(data: any): void {
        this._spineStates = new Map<String, SpineState>();
        let spineState: SpineState;

        for (let state in data) {
            if (data.hasOwnProperty(state)) {

                //TODO: is this safe? will it always point to first element in yaml?
                if (!this._defaultStateName) {
                    this._defaultStateName = state;
                    this._currentStateName = state;
                }

                spineState = new SpineState(state, data[state]);
                spineState.on(SpineState.SHOW_STATE, this._onSpineStateShow, this);

                this._spineStates.set(state, spineState);
            }
        }
    }

    private _onSpineStateShow(data: any) {
        this._currentStateName = data.state;
        this._showState();
    }

    private _showState() {
        const state: SpineState = this._spineStates.get(this._currentStateName);

        if (!state) {
            throw new Error("SpineContainer :: No state with name: " + this._currentStateName);
        }

        if (this.hasSyncManager) {
            this._currentAnimationName = SpineSyncManager.getAnimationName(state);
        } else {
            this._currentAnimationName = state.animationNames[Math.floor(Math.random() * state.animationNames.length)];
        }

        if (!this._spine.state.hasAnimation(this._currentAnimationName)) {
            throw new Error("SpineContainer :: No animation with name: " + this._currentAnimationName);
        }

        this._showAnimation();
    }

    private _showAnimation() {

        const state: SpineState = this._spineStates.get(this._currentStateName);
        let animation: TrackEntry;

        if (this.animationCrossfadeTime > 0) {
            animation = this._spine.state.setAnimation(0, this._currentAnimationName, state.loop);
            animation.onComplete = () => {
                this.emit(SpineContainer.ANIMATION_COMPLETE, {"test": "test123"});
                animation.onComplete = null;
            };
        } else {
            animation = this._spine.state.addAnimation(0, this._currentAnimationName, false, 0);

            animation.onComplete = () => {

                animation.onComplete = null;

                this._spine.state.clearTracks();

                if (state.loop && animation.animation.duration > 0) {
                    this._showAnimation();
                } else {
                    this._currentStateName = state.onComplete && animation.animation.duration > 0 ? state.onComplete : this._defaultStateName;
                    this._showState();
                }

                this.emit(SpineContainer.ANIMATION_COMPLETE);
            };
        }

    }

    private _onSpineEventTriggered(i: any, event: any) {
        this.emit(SpineContainer.SPINE_EVENT, {id: event.data.name});
    }

    private _update(deltaTime: number = 0.001) {
        this._spine.update(deltaTime / 60);
    }

    /**
     * provides information is spine animation is active
     */
    get running(): boolean {
        return this._running;
    }

    /**
     * sets the states
     */
    public set states(states: any) {
        this._generateSpineStates(states);
        this._cfgStates = states;
    }

    /**
     * returns map of spine states
     */
    get spineStates(): ReadonlyMap<String, SpineState> {
        return this._spineStates;
    }

    /**
     * returns currently active animation name
     */
    get currentAnimationName(): string {
        return this._currentAnimationName;
    }

    /**
     * returns currently active state name
     */
    get currentStateName(): string {
        return this._currentStateName;
    }

    /**
     * returns default state name
     */
    get defaultStateName(): string {
        return this._defaultStateName;
    }

}
