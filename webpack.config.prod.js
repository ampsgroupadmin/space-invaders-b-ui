const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ZipPlugin = require('zip-webpack-plugin');

module.exports = (function (options) {
	return {
		entry: {
			main: path.resolve("./src/index.ts")
		},
		
		node: {
			fs: 'empty'
		},
		
		output: {
			path: __dirname + "/deploy/build/",
			filename: "bundle.js",
			library: 'Wheel'
		},
		
		devtool: 'source-map',
		
		module: {
			rules: [
				{test: /\.ts$/, loader: "ts-loader"}
			]
		},
		
		plugins: [
			new HtmlWebpackPlugin({
				title: 'LVBet Wheel of Fortune'
			}),
			new CopyWebpackPlugin([
				{from: 'assets', to: 'assets'}
			]),
			new ZipPlugin({
				path: '../dist/',
				exclude: [/\.zip/, /\.ltr/],
				filename: 'wheel-module.zip'
				
			})
		],
		
		resolve: {
			extensions: ['.ts', '.js', '.json']
		}
		
	}
})();
